package com.sortingApp.app;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class SortingTest 
{
    
    Sorting sorting = new Sorting();

    @Test (expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    
    @Test
    public void testEmptyCase(){
        int[] array = {};
        sorting.sort(array);
        assertArrayEquals(new int[]{}, array);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] array = {1};
        sorting.sort(array);
        assertArrayEquals(new int[]{1}, array);
    }

    @Test
    public void testSortedArraysCase() {
        int[] array = {1,2,3,4,5};
        sorting.sort(array);
        assertArrayEquals(new int[]{1,2,3,4,5}, array);
    }

    @Test
    public void testOtherCases() {
        int[] array = {5,3,4,2,1};
        sorting.sort(array);
        assertArrayEquals(new int[]{1,2,3,4,5}, array);
    }
}

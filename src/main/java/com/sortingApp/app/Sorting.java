package com.sortingApp.app;

public class Sorting
{
    public void sort( int[] array )
    {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        
        for (int i = 0; i < array.length; i++){
            int menor = i;
            int temp = 0;
            int j;
            for (j = i; j < array.length; j++) {
                if(array[menor] >= array[j]){
                    menor = j;
                }
            }
            temp = array[i];
            array[i] = array[menor];
            array[menor] = temp;
        }
    }
}
